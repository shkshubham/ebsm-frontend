export default {
  items: [
    {
      name: 'Course',
      url: '/',
      icon: 'icon-list',
    },
    {
      name: 'Class',
      url: '/class/list',
      icon: 'icon-list',
    }
  ],
};
