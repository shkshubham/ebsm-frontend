export const LOGIN = 'LOGIN_USER';
export const PING = 'PING';
export const LOGOUT = 'LOGOUT';
export const REGISTER = "REGISTER";
export const CLEAR_REGISTER = "CLEAR_REGISTER";
export const CLEAR_LOGIN = "CLEAR_LOGIN";

export const GET_CLASS_LIST = "GET_CLASS_LIST";
export const SHOW_CLASS = "SHOW_CLASS";
// export const CREATE_CLASS = "CREATE_CLASS";
// export const UPDATE_CLASS = "UPDATE_CLASS";
// export const DELETE_CLASS = "DELETE_CLASS";


export const GET_STUDENT_LIST = "GET_STUDENT_LIST";
// export const CREATE_STUDENT = "CREATE_STUDENT";
// export const UPDATE_STUDENT= "UPDATE_STUDENT";
// export const DELETE_STUDENT = "DELETE_STUDENT";

export const GET_COURSE_LIST = "GET_COURSE_LIST";
export const SHOW_COURSE = "SHOW_COURSE";
// export const CREATE_COURSE = "CREATE_COURSE";
// export const UPDATE_COURSE = "UPDATE_COURSE";
// export const DELETE_COURSE = "DELETE_COURSE";
