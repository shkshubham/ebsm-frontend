import CommonAPI from '../apis/CommonAPI';
// import { GET_COURSE_LIST, CREATE_COURSE, UPDATE_COURSE, DELETE_COURSE, SHOW_COURSE } from './types';
import { GET_COURSE_LIST, SHOW_COURSE } from './types';

const commonAPI = new CommonAPI("course");
export const listCourseAction = () => {
    const payload = commonAPI.listAPI();
    return {
        type: GET_COURSE_LIST,
        payload
    }
}
// export const createCourseAction = (data) => {
//     const payload = commonAPI.createAPI(data);
//     return {
//         type : CREATE_COURSE,
//         payload
//     }
// }
// export const updateCourseAction = (data) => {
//     const payload = commonAPI.updateAPI(data);
//     return {
//         type : UPDATE_COURSE,
//         payload
//     }
// }
// export const deleteCourseAction = (id) => {
//     const payload = commonAPI.createAPI(id);
//     return {
//         type : DELETE_COURSE,
//         payload
//     }
// }
export const showCourseAction = (id) => {
    const payload = commonAPI.showAPI(id);
    return {
        type : SHOW_COURSE,
        payload
    }
}