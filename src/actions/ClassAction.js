import CommonAPI from '../apis/CommonAPI';
import { GET_CLASS_LIST, SHOW_CLASS } from './types';
// import { GET_CLASS_LIST, SHOW_CLASS , UPDATE_CLASS, DELETE_CLASS } from './types';

const commonAPI = new CommonAPI("class");

export const listClassAction = () => {
    const payload = commonAPI.listAPI();
    return {
        type: GET_CLASS_LIST,
        payload
    }
}
// export const createCourseAction = (data) => {
//     const payload = commonAPI.createAPI(data);
//     return {
//         type : CREATE_COURSE,
//         payload
//     }
// }
// export const updateCourseAction = (data) => {
//     const payload = commonAPI.updateAPI(data);
//     return {
//         type : UPDATE_COURSE,
//         payload
//     }
// }
// export const deleteCourseAction = (id) => {
//     const payload = commonAPI.createAPI(id);
//     return {
//         type : DELETE_COURSE,
//         payload
//     }
// }
export const showClassAction = (id) => {
    const payload = commonAPI.showAPI(id);
    return {
        type : SHOW_CLASS,
        payload
    }
}