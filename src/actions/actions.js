import {
    loginAction,
    pingAction,
    logoutAction,
    registerAdminAction,
    clearRegisterAction,
    clearLoginAction
} from './AuthAction';

import {
    listCourseAction, showCourseAction
} from './CourseAction';
import {
    listClassAction, showClassAction
} from './ClassAction';

export const loginAdmin = loginAction;
export const getPing = pingAction;
export const logoutAdmin = logoutAction;
export const registerAdmin = registerAdminAction;
export const clearRegister = clearRegisterAction;
export const clearLogin = clearLoginAction;

export const listCourse = listCourseAction;
export const showCourse = showCourseAction;

export const listClass = listClassAction;
export const showClass = showClassAction;