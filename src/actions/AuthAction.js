import { loginAPI, pingAPI,
    logoutAPI, registerAPI
} from '../apis/AuthAPI';
import { LOGIN, PING, LOGOUT, REGISTER, CLEAR_REGISTER, CLEAR_LOGIN
} from './types';
export function loginAction(userData) {
    const payload = loginAPI(userData);
    return {
        type: LOGIN,
        payload
    }
}
export function pingAction(){
    const payload = pingAPI();
    return {
        type: PING,
        payload
    }
}

export function logoutAction(){
    const payload = logoutAPI();
    return {
        type: LOGOUT,
        payload
    }
}

export function registerAdminAction(data){
    const payload = registerAPI(data);
    return {
        type: REGISTER,
        payload
    }
}

export function clearRegisterAction(){
    return {
        type: CLEAR_REGISTER,
    }
}

export function clearLoginAction(){
    return {
        type: CLEAR_LOGIN,
    }
}