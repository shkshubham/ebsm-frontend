import React from 'react';
import Loadable from 'react-loadable'

function Loading() {
  return <div>Loading...</div>;
}

const Dashboard = Loadable({
  loader: () => import('../components/Dashboard/Dashboard'),
  loading: Loading,
});

const Course = Loadable({
  loader: () => import('../components/Course/course'),
  loading: Loading,
});
const Class = Loadable({
  loader: () => import('../components/Class/class'),
  loading: Loading,
});

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  // { path: '/dashboard', exact: true, name: 'Dashboard', component: Dashboard },
  // { path: '/course/list', exact: true, name: 'Course List', component: Course },
  { path: '/course/show/:id', exact: true, name: 'Course Show', component: Course },
  { path: '/class/list', exact: true, name: 'Class List', component: Class },
  { path: '/class/show/:id', exact: true, name: 'Class Show', component: Class },
  { path: '/', exact: true, name: 'Course List', component: Course },
];

export default routes;
