import { GET_CLASS_LIST, SHOW_CLASS } from '../actions/types';

const initialState = null;
export function getClassReducer(state = initialState, action){
    switch (action.type) {
        case GET_CLASS_LIST:
            if (action.payload.status === 200) {
                return action.payload.data.data;
            }
            return {
               ...state
            }
        default:
            return state;
    }
}

export function getClassDetailReducer(state = initialState, action){
    switch (action.type) {
        case SHOW_CLASS:
            if (action.payload.status === 200) {
                return action.payload.data.data;
            }
            return {
               ...state
            }
        default:
            return state;
    }
}