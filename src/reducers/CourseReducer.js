import { GET_COURSE_LIST, CREATE_COURSE, DELETE_COURSE, UPDATE_COURSE, SHOW_COURSE } from '../actions/types';

const initialState = null;
export function getCourseReducer(state = initialState, action){
    switch (action.type) {
        case GET_COURSE_LIST:
            if (action.payload.status === 200) {
                return action.payload.data.data;
            }
            return {
               ...state
            }
        default:
            return state;
    }
}

export function getCourseDetailReducer(state = initialState, action){
    switch (action.type) {
        case SHOW_COURSE:
            if (action.payload.status === 200) {
                return action.payload.data.data;
            }
            return {
               ...state
            }
        default:
            return state;
    }
}