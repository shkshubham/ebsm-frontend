import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'
import authReducer, { registeredReducer } from './AuthReducer';
import {
    getCourseReducer, getCourseDetailReducer
} from './CourseReducer';
import {
    getClassDetailReducer, getClassReducer
} from './ClassReducer';
export default combineReducers({
    user: authReducer,
    registered: registeredReducer,
    course: getCourseReducer,
    courseDetails: getCourseDetailReducer,
    classInfo: getClassReducer,
    classDetails: getClassDetailReducer,
    form: formReducer,
});