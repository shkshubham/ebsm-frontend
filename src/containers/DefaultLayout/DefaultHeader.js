import React, { Component } from 'react';
import { DropdownItem, DropdownMenu, DropdownToggle, Nav } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import '../DefaultLayout/DefultLayout.css';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import { logoutAdmin } from '../../actions/actions';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {

  logout = () => {
    this.props.logoutAdmin()
  }

  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
       
        {/* <AppSidebarToggler className="d-md-down-none" display="lg" /> */}
        <Nav className="ml-auto" navbar>
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <DropdownItem onClick={() => this.logout()}><i className="fa fa-lock"></i> Logout</DropdownItem>
            </DropdownToggle>
          </AppHeaderDropdown>
        </Nav>
        {/* <AppAsideToggler className="d-md-down-none" /> */}
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

function mapStateToProps(state) {
  return {
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    logoutAdmin
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultHeader)
