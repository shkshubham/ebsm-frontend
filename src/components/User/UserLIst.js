import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, CardBody, CardHeader, Col, Row, Table, Badge } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { getUserList } from '../../actions/UserAction';


class UserList extends Component {

    userList() {
        if (this.props.UserList) {
            return this.props.UserList.map(user => {
                if (user.firstName || user.email || user.mobile_number) {
                    return (
                        <tr key={user.id}>
                            <td>
                                {user.firstName == null ? "-" : user.firstName} {user.lastName}
                            </td>
                            <td>
                                {user.email == null ? "-" : user.email}
                            </td>
                            <td>
                                {user.mobile_number == null ? "-" : user.mobile_number}
                            </td>
                            <td className="text-center">
                                <Row>{this.getDogs(user.dogs)}</Row>
                            </td>
                        </tr>
                    )
                }
            })
        }
    }

    getDogs = (dogs) => {
        if (dogs.length > 0) {
            return dogs.map(dog => {
                return (
                    <Col md="3" id={dog.id}>
                        {
                            dog.image != "" ? <div><img src={dog.image} width="50" className="img-circle" /><br /></div> : ""
                        }
                        {dog.name}
                    </Col>
                )
            })
        } else {
            return (
                <Col md="3">
                    -
                </Col>
            )
        }
    }

    componentWillMount() {
        this.props.getUserList();
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col xs="12" lg="12">
                        <Card>
                            <CardHeader>
                                User List
                            </CardHeader>
                            <CardBody>
                                <Table responsive>
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Dog</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.userList()}
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        UserList: state.UserList
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getUserList
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList)