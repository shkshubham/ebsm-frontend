import React, { Component } from 'react';
import './dashboard.css';
import Course from '../Course/course';
class Dashboard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="dashboard-section">
          <Course />
      </div>
    );
  }
}

export default Dashboard;
