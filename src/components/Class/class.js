import React, { Component } from 'react';
import { Row } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  listClass, showClass
} from '../../actions/actions';
import List from '../Include/CRUD/list';

class Class extends Component {
    constructor(props){
        super(props);
        this.state = {
            tableHeader: [],
            title: "",
            isShow: false,
            tableData: null,
            type: ""
        }
    }
    onClickTable = (id, type) => {
       this.props.history.push(`/${type}/show/${id}`)
    }
    componentWillMount(){
        const {
            pathname
        } = this.props.history.location
        if(pathname === '/class/list'){
            this.props.listClass()
        }
        else{
            this.props.showClass(this.props.match.params.id)
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.classDetails && nextProps.classDetails !== this.props.classDetails){
            this.setState({
                title: `Class: ${nextProps.classDetails.name}`,
                tableHeader: [ "Student Roll No", "id", "Student name", ],
                tableData: nextProps.classDetails.students,
                type: "student"
            })
        }

        if(nextProps.classInfo && nextProps.classInfo !== this.props.classInfo){
            this.setState({
                title: `Class`,
                tableHeader: [ "id", "Class name" ],
                tableData: nextProps.classInfo,
                type: "class"
            })
        }
    }
    render() {
        console.log(this.state)
        const { course } = this.props;
        const { tableHeader, title, tableData, type } = this.state;
        return (
            <div className="animated fadeIn">
                <Row>
                    {
                        tableData
                        ? 
                        <List 
                            onClickTable={this.onClickTable}
                            title={title}
                            tableHeader={tableHeader}
                            tableData={tableData}
                            type={type}
                        /> : null
                    }
                </Row>
            </div>
        )
    }
}

function mapStateToProps(state) {
  const {
    classInfo,
    classDetails
  } = state
  return {
    classInfo,
    classDetails
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    listClass,
    showClass
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Class);