import React, { Component } from 'react';
import { Card, CardBody, Col, Row, CardHeader, Table } from 'reactstrap';
class List extends Component {
    renderTableHeader = (tableHeader) => {
        return tableHeader.map((header, index)=> {
            return <th key={`${index}_${header}`}>{header}</th>
        })
    }
    renderBody = (tableData) => {
        const {
            type
        } = this.props;
        if(tableData.length > 0){
            return tableData.map((data)=> {
                const {
                    id,
                    name
                } = data;
                return (
                    <tr style={ type !== "student" ? {
                        cursor: "pointer"
                    }: {}} onClick={type !== "student" ? () => this.props.onClickTable(id, type) : null} key={id}>
                        <td>{id}</td>
                        <td>{name}</td>
                        {
                            type === "student" ? <td>{data.roll_no}</td> : null
                        }
                    </tr>
                )
            })
        }
        else {
            return (
                <tr>
                    <td></td>
                    <td>No record found</td>
                    {
                        type === "student" ? <td></td> : null
                    }
                </tr>
            )
        }
        
    }
    render() {
      const { 
          title,
          tableHeader,
          tableData
      } = this.props;
    return (
        <Col xs="12" lg="12">
            <Card>
                <CardHeader>
                    { title }
                </CardHeader>
                <CardBody>
                    <Table responsive>
                        <thead>
                            <tr>
                                { this.renderTableHeader(tableHeader) }
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderBody(tableData)}
                        </tbody>
                    </Table>
                </CardBody>
            </Card>
        </Col>
    )
  }
}


export default List;