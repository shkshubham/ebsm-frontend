import React, { Component } from 'react';
import { Card, CardBody, CardGroup, Col, Container, Row } from 'reactstrap';
import { connect } from 'react-redux';
import {
    Link
  } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import '../Login/login.css';
import { isEmailCheck } from '../../helpers/helper';
import { renderFields } from '../../helpers/fields';
import {
  registerAdmin, clearRegister
} from '../../actions/actions';
import { Button } from 'reactstrap';
import { Field, reduxForm } from 'redux-form';
import { toast } from 'react-toastify';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Register extends Component {

  
  onLoginSubmit = (values) => {
    this.props.clearRegister();
    this.props.registerAdmin(values);
  }
  componentWillMount() {
    if (this.props.user.isLoggedIn) {
      this.props.history.push('/')
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.registered.isRegistered === true && this.props.registered.isRegistered !== nextProps.registered.isRegistered) {
        toast.info(`${nextProps.registered.msg}`);
    }
    else if(nextProps.registered.isRegistered === false && this.props.registered.isRegistered !== nextProps.registered.isRegistered){
        toast.info(`${nextProps.registered.msg}`);
    }
  }
  render() {
    const { handleSubmit, pristine, submitting } = this.props;
    return (
      <div className="login app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <form onSubmit={handleSubmit(this.onLoginSubmit.bind(this))}>
                      <h1>Register</h1>
                      <p className="text-muted">Register</p>
                      <Field 
                          className="mb-3"
                          name="name" 
                          label="Name"
                          icon="icon-user"
                          component={renderFields} 
                          type="text" 
                          placeholder="Name" 
                      />
                      <Field 
                          className="mb-3"
                          name="email" 
                          label="Email"
                          icon="icon-user"
                          component={renderFields} 
                          type="text" 
                          placeholder="Email" 
                      />
                       <Field 
                          className="mb-4"
                          name="password" 
                          label="Password"
                          icon="icon-lock"
                          component={renderFields} 
                          type="password" 
                          placeholder="Password" 
                      />
                      <Row>
                        <Col xs="6">
                          {
                              this.props.registered.msg.length > 1 ?
                              this.props.registered.msg : null
                          }
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="6">
                          Already have account <Link to="/login">Login</Link>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" type="submit" disabled={pristine || submitting} className="px-4">Register</Button>
                        </Col>
                      </Row>
                    </form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
          <ToastContainer
            position="bottom-left"
            autoClose={2000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnVisibilityChange
            draggable
            pauseOnHover
         />
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const {
    user,
    registered
  } = state
  return {
    user,
    registered
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    registerAdmin,
    clearRegister
  }, dispatch)
}

function validate(values){
  const errors= {};
  if(!values.username){
      errors.username = "Please enter your email address";        
  }
  else if(!isEmailCheck(values.username)){
    errors.username = "Please enter valid email address";        
  }
  if(!values.password){
      errors.password = "Please enter your password";        
  }

  if(!values.name){
    errors.name = "Please enter your name";        
}
  return errors;
}

export default reduxForm({
  validate,
  form: 'register', // a unique identifier for this form
})(connect(mapStateToProps, mapDispatchToProps)(Register))