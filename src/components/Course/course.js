import React, { Component } from 'react';
import { Row } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  listCourse, showCourse
} from '../../actions/actions';
import List from '../Include/CRUD/list';

class Course extends Component {
    constructor(props){
        super(props);
        this.state = {
            tableHeader: [],
            title: "",
            isShow: false,
            tableData: null,
            type: ""
        }
    }
    onClickTable = (id, type) => {
       this.props.history.push(`/${type}/show/${id}`)
    }
    componentWillMount(){
        const {pathname} = this.props.history.location
        if(pathname === '/course/list' || pathname === '/'){
            this.props.listCourse()
        }
        else if(!this.props.history) {
            this.props.listCourse()
        }
        else{
            this.props.showCourse(this.props.match.params.id)
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.courseDetails && nextProps.courseDetails !== this.props.courseDetails){
            this.setState({
                title: `Course: ${nextProps.courseDetails.name}`,
                tableHeader: [ "id", "Class name" ],
                tableData: nextProps.courseDetails.classes,
                type: "class"
            })
        }

        if(nextProps.course && nextProps.course !== this.props.course){
            this.setState({
                title: `Course`,
                tableHeader: [ "id", "Course name" ],
                tableData: nextProps.course,
                type: "course"
            })
        }
    }
    render() {
        const { course } = this.props;
        const { tableHeader, title, tableData, type } = this.state;
        return (
            <div className="animated fadeIn">
                <Row>
                    {
                        tableData
                        ? 
                        <List 
                            onClickTable={this.onClickTable}
                            title={title}
                            tableHeader={tableHeader}
                            tableData={tableData}
                            type={type}
                        /> : null
                    }
                </Row>
            </div>
        )
    }
}

function mapStateToProps(state) {
  const {
    course,
    courseDetails
  } = state
  return {
    course,
    courseDetails
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    listCourse,
    showCourse
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Course);